defmodule EngineInterfaceWeb.PageControllerTest do
  use EngineInterfaceWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")

    assert html_response(conn, 200) =~
             "We're sorry but web-client doesn't work properly without JavaScript enabled"
  end
end
