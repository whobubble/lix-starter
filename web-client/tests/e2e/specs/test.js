// https://docs.cypress.io/api/introduction/api.html

describe("Counter Engine", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Welcome to Your Vue.js App");
  });
  it("Should open counter page", () => {
    cy.get('[href="#/counters"]').click();
    cy.contains("h1", "This is a page with counters");
  });
  it("Should create counter asdf", () => {
    cy.get('[href="#/counters"]').click();
    cy.get("[data-test=counterNameInput]").type("asdf");
    cy.get("[data-test=createCounterButton]").click();
    cy.contains("Counter asdf");
    cy.get("[data-test=counterInput]").should("have.value", "3");
    cy.get('[data-test="incButton"]').click();
    cy.get("[data-test=counterInput]").should("have.value", "4");
    cy.get("[data-test=decButton]").click();
    cy.get("[data-test=counterInput]").should("have.value", "3");
  });
});
