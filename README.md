Lix-Starter is a batteries-included repo to kick start any Elixir/Vue project from.

It includes:

- One Elixir OTP application (A1) for core logic (only contains one module)
  - The app is configured with adequate development dependencies
- An Phoenix interface application (A2) that includes A1 as a dependency
- Channel setup to be used with phoenix.js from a Vue application
- Basic Vue application (configurable with vue ui or cli) that can interface with backend through Phoenix channel
- Bulma setup of CSS template
- Instantly deployable to gigalixir

Options that are not included

- Prepared option of vault as external auth provider
- Prepared option to integrate blockchain platforms like Waves
- Prepared option to integrate PostgreSQL setup (useable via Gigalixir)
- Prepared option to integrate GraphQL

Design problems:

- templating (Vue Single file Components)
- free wheeling (Bulma)

to add:

- bulma
- cypress.js
- ecto
- cordova
- elixir unit and doc tests
- elixir property tests


Troubleshooting
* resource links need to be redirected and kept up to date
* cypress.js --record option and usage of dashboard
** Cypress uses 2 main modes either cypress run to use a headless Chrome or cypress open to start a UI
** Recording is easy when using cypress run with these commands:
** set CYPRESS_RECORD_KEY "9a40bced-737f-44fa-beb3-31db6984c76e"
** npx cypress run --record --key <KEY>
* deeper understanding of how to write and test a GenServer
** https://medium.com/blackode/live-example-of-genserver-and-genserver-testing-ed55c8eb4f76
* bulma resources
* bulma quick start (admin?)
* deployment
* setting up account with Gigalixir
* setting up account on Gitlab, cloning the starter repo
* eschewing Vuex and using just Vue with Channels all the way (this has limitations)
* getting started with Ecto
* fusing vue onto Phoenix
* using doctest, unit tests and property tests with coverage in Elixir (plus how to track somewhere)
* using Cypress to test UI and how to use the Dashboard
* Using vscode LIve Share in order to mentor/collaborate with other devs on the team
* using Elixir dev tools like inch_ex etc. consistently
* Making a mobile app from a vue application using Cordova
* Setting up VS code for using lix-starter